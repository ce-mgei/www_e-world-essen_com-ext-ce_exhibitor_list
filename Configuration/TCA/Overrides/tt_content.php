<?php
// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    [
        'ExhibitorList', // 'LLL:EXT:your_extension_key/Resources/Private/Language/Tca.xlf:yourextensionkey_newcontentelement',
        'ce_exhibitor_list',
        'EXT:ce_exhibitor_list/Resources/Public/Icons/Extension.svg'
    ],
    'CType',
    'ce_exhibitor_list'
);

// Configure the default backend fields for the content element
// --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.header;header

$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds']['*,ce_exhibitor_list'] = 'FILE:EXT:ce_exhibitor_list/Configuration/FlexForm/FlexForm.xml';
$GLOBALS['TCA']['tt_content']['types']['ce_exhibitor_list'] = [
    'showitem' => '
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
         header, bodytext, pi_flexform,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.frames;frames,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.access,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.visibility;visibility,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.access;access,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.extended
    ',
    'columnsOverrides' => [
        'assets' => [
            'config' => [
                'overrideChildTca' => [
                    'columns' => [
                        'crop' => [
                            'config' => [
                                'cropVariants' => [
                                    'default' => [
                                        'disabled' => true,
                                    ],
                                    'mobile' => [
                                        'disabled' => true,
                                    ],
                                    'desktop' => [
                                        'disabled' => true,
                                    ],
                                    'teaser' => [
                                        'title' => 'Teaserbox-Bild', // 'LLL:EXT:ext_key/Resources/Private/Language/locallang.xlf:imageManipulation.mobile',
                                        'cropArea' => [
                                            'x' => 0.1,
                                            'y' => 0.1,
                                            'width' => 0.8,
                                            'height' => 0.8,
                                        ],
                                        'allowedAspectRatios' => [
                                            '2x1' => [
                                                'title' => '2x1',
                                                'value' => 2 / 1
                                            ],
                                            'NaN' => [
                                                'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                                                'value' => 0.0
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ]
    ]
];
