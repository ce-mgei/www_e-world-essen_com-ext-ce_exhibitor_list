<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'ce_exhibitor_list',
    'description' => 'ce_exhibitor_list',
    'category' => 'fe',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author_company' => 'con|energy agentur',
    'author_email' => 'agentur-internet@conenergy.com',
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
            
        ],
        'conflicts' => [],
        'suggests' => [
            'ot_bootstrap3' => '8.7.0-8.9.99'
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'ConEnergy\\CeExhibitorList\\' => 'Classes'
        ]
    ]
];
