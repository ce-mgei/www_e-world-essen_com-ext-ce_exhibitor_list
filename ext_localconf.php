<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

/**
 * Icon Factory
 *
 * @see https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/Icon/Index.html
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'ce_exhibitor_list',
    \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
    [
        'name' => 'star',
        'spinning' => false
    ]
);

// <INCLUDE_TYPOSCRIPT: source="FILE:EXT:my_extension_key/Configuration/TypoScript/pageTsConfig.ts">
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
mod.wizards.newContentElement.wizardItems.extras {
    header = Extras
    elements {
        ot_bt3carousel {
            iconIdentifier = ce_exhibitor_list
            
            # You can use: LLL:EXT:your_extension_key/Resources/Private/Language/Tca.xlf:yourextensionkey_newcontentelement.wizard.title
            title = Exhibitor List
             
            # LLL:EXT:your_extension_key/Resources/Private/Language/Tca.xlf:yourextensionkey_newcontentelement.wizard.description
            description = Teaserbox
        }
   }
   show := addToList(ce_exhibitor_list)
}
');

